(function(window) {
    window.greenpepperJS = {
        toggleStackTrace: toggleStackTrace
    };

    function toggleStackTrace(button) {
        var stacktraces = button.parentElement.getElementsByClassName("greenpepper-report-stacktrace");
        for (var i = 0; i < stacktraces.length; i++) {
            var stacktrace = stacktraces[i];
            var isShown = stacktrace.classList.toggle("show");
            if (isShown) {
                button.textContent = 'v';
            } else {
                button.textContent = '>';
            }

        }
    }

})(this);