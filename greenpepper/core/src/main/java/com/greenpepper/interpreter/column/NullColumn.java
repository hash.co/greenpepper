
/**
 * <p>NullColumn class.</p>
 *
 * @author oaouattara
 * @version $Id: $Id
 */
package com.greenpepper.interpreter.column;

import com.greenpepper.Example;
import com.greenpepper.Statistics;
public class NullColumn extends ExpectedColumn {

    public NullColumn() {
        super((String) null);
    }

    /** {@inheritDoc} */
    public Statistics doCell( Example cell )
    {
        return new Statistics();
    }
}
