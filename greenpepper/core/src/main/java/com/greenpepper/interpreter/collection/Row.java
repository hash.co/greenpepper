package com.greenpepper.interpreter.collection;

import com.greenpepper.Call;
import com.greenpepper.Example;
import com.greenpepper.Statistics;
import com.greenpepper.expectation.Expectation;
import com.greenpepper.expectation.ShouldBe;
import com.greenpepper.interpreter.HeaderForm;
import com.greenpepper.interpreter.column.Column;
import com.greenpepper.interpreter.column.ExpectedColumn;
import com.greenpepper.reflect.Fixture;
import com.greenpepper.util.StringUtil;

import static com.greenpepper.util.ExampleUtil.contentOf;

/**
 * <p>Row class.</p>
 *
 * @author oaouattara
 * @version $Id: $Id
 */
public class Row
{
    private final Fixture fixture;
    private final Column[] headers;

    /**
     * <p>Constructor for Row.</p>
     *  @param fixture a {@link Fixture} object.
     * @param headers a {@link Example} object.
     */
    public Row(Fixture fixture, Column[] headers)
    {
        this.headers = headers;
        this.fixture = fixture;
    }

    /**
     * <p>parse.</p>
     *
     * @param fixture a {@link com.greenpepper.reflect.Fixture} object.
     * @param headers a {@link com.greenpepper.Example} object.
     * @return a {@link com.greenpepper.interpreter.collection.Row} object.
     */
    public static Row parse(Fixture fixture, Column[] headers)
    {
        return new Row( fixture, headers );
    }

    /**
     * <p>matches.</p>
     *
     * @param cells a {@link com.greenpepper.Example} object.
     * @return a boolean.
     */
    public boolean matches(Example cells)
    {
        for (int i = 0; i < cells.remainings(); ++i)
        {
            Example cell = cells.at( i );

            if (i < headers.length)
            {
                ExpectedColumn column = (ExpectedColumn) headers[i];
                String message = column.header();

                if (HeaderForm.parse(message).isExpected()) continue;
                if (StringUtil.isBlank( contentOf( cell ) )) {
                    if (column.getClass() == ExpectedColumn.class) continue;

                    try
                    {
                        column.setCheck(fixture.check(column.header()));
                        final Statistics statistics = column.doCell(cell, false);

                        if (statistics.indicatesFailure())
                        {
                            return false;
                        }
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                } else { // Check for equality
                    try
                    {
                        Call call = new Call( fixture.check( column.header() ) );
                        Expectation expectation = ShouldBe.literal( contentOf( cell ) );

                        Object result = call.execute();
                        if (!expectation.meets( result ))
                        {
                            return false;
                        }
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }


            }
        }

        return true;
    }
}
