package com.greenpepper.interpreter;

import com.greenpepper.Example;
import com.greenpepper.Statistics;
import com.greenpepper.annotation.Annotations;
import com.greenpepper.interpreter.column.Column;
import com.greenpepper.interpreter.column.NullColumn;
import com.greenpepper.reflect.Fixture;

import static com.greenpepper.GreenPepper.shouldStop;
import static com.greenpepper.annotation.Annotations.exception;

/**
 * Interpreter that handles common to every table like examples, that is:
 * <ul>
 *     <li>{@link SetupInterpreter}</li>
 *     <li>{@link RuleForInterpreter}</li>
 *     <li>{@link ListOfInterpreter}</li>
 *     <li>{@link SetOfInterpreter}</li>
 *     <li>{@link SubsetOfInterpreter}</li>
 *     <li>{@link SupersetOfInterpreter}</li>
 * </ul>
 */
public abstract class AbstractTableInterpreter extends AbstractInterpreter {
    protected final Fixture fixture;
    protected Column[] columns;
    protected Statistics stats = new Statistics();

    protected AbstractTableInterpreter(Fixture fixture) {
        this.fixture = fixture;
    }

    protected Column[] parseColumns(Example table)
    {
        Example headers = table.at( 0, 1, 0 );
        if (headers == null) return new Column[0];

        Column[] columns = new Column[headers.remainings()];
        for (int i = 0; i < headers.remainings(); i++)
        {
            columns[i] = parseColumn( headers.at( i ) );
        }

        if (shouldStop( stats ))
        {
            headers.lastSibling().addSibling().annotate(Annotations.stopped());
        }

        return columns;
    }

    protected Column parseColumn(Example header )
    {
        try
        {
            return HeaderForm.parse(header.getContent()).selectColumn(fixture);
        }
        catch (Exception e)
        {
            header.annotate( exception( e ) );
            stats.exception();
            return new NullColumn();
        }
    }

    protected void doCell(Column column, Example cell)
    {
        try
        {
            stats.tally( column.doCell( cell ) );
        }
        catch (Exception e)
        {
            cell.annotate( exception( e ) );
            stats.exception();
        }
    }
}
