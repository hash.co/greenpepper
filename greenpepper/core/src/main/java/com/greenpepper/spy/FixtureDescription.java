package com.greenpepper.spy;

import java.util.Collection;

/**
 * This interface provides access to the datas that were guessed when we spied on the Specification.
 */
public interface FixtureDescription extends SpyDescription {

    /**
     * The different types of fixtures.
     * <ul>
     *     <li>COLLECTION_PROVIDER: used with <code>list of</code>, <code>set of</code>, <code>subset of</code>, <code>superset of</code></li>
     *     <li>SETUP: used with <code>Setup</code></li>
     *     <li>WORKFLOW: used with <code>do with</code>, <code>Scenario</code></li>
     * </ul>
     */
    enum FixtureType {
        COLLECTION_PROVIDER, SETUP, WORKFLOW
    }

    /**
     * @return The list of all methods detected.
     */
    Collection<MethodDescription> getMethods();

    /**
     * @return The list of all constructors guessed.
     */
    Collection<ConstructorDescription> getConstructors();

    /**
     * @return the list of all properties.
     */
    Collection<PropertyDescription> getProperties();

    /**
     * @return the type of the fixture.
     */
    FixtureType getType();

    /**
     * @return the POJO returned by a COLLECTION_PROVIDER typed fixture.
     */
    PojoDescription getPojo();
}
