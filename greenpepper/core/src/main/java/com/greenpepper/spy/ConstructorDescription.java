package com.greenpepper.spy;

public interface ConstructorDescription {

    /**
     * The Camel Case name of the constructor in JAVA convention.
     */
    String getName();

    /**
     *
     * @return the number of argument of that constructor.
     */
    int getArity();
}
