package com.greenpepper.spy;

import java.util.List;

/**
 * Information related to the specification but not directly linked to a FixtureDescription.
 */
public interface MetaInformation {

    /**
     * @return the list of imports present in the specification.
     */
    List<String> getImports();
}
