package com.greenpepper.spy;

public interface MethodDescription extends SpyDescription {

    /**
     * The number of parameters that this method takes.
     *
     * @return the arity. 0 means no parameter.
     */
    int getArity();

    /**
     * In some cases, a method is a new Fixture imbricated in another. This is the case for Setups, List of, Set of when
     * they are children of a <code>do with</code>
     *
     * @return the Sub Fixture Description.
     */
    FixtureDescription getSubFixtureDescription();
}
