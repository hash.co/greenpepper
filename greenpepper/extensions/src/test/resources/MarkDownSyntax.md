# Calculator Rules

| rule for | com.greenpepper.runner.ant.Calculator |
| x  |  y | sum? | product? | quotient? |
|----|----|------|----------|-----------|
| 6  | 2 | 8 | 12 | 3 |


# Banker

| do with | com.greenpepper.runner.dialect.BankerFixture |
|---------|--------|
| accept | open an account for | John |
| display | the id of | John | account |
| check  | the balance of | John | account is | 0 |

