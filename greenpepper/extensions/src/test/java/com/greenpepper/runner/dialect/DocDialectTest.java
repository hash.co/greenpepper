package com.greenpepper.runner.dialect;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

public class DocDialectTest {

    @Test
    public void shouldConvertCalculatorDoc() throws IOException {
        InputStream resourceAsStream = getClass().getResourceAsStream("calculator.doc");

        DocDialect dialect = new DocDialect();
        String convert = dialect.convert(resourceAsStream);

        Document parse = Jsoup.parse(convert);
        Elements tables = parse.body().getElementsByTag("table");
        assertEquals(4, tables.size());
        assertEquals("Begin Info", tables.get(0).text());
        assertEquals("End Info", tables.get(1).text());
    }

}