# Support for Markdown syntax

[Markdown]: https://daringfireball.net/projects/markdown/
[CommonMark]: http://commonmark.org/

Using the `--dialect` option in greenpepper, you can define a class that will be
used to convert a specification from a syntax to HTML. 

That is what is used for greenpepper to support [Markdown].

## Example usage

To use the default Markdown dialect, you will need to include `greenpepper-extensions-java` 
in your classpath. Then you need to use the class `com.greenpepper.runner.dialect.MarkdownDialect`.

```bash
java -cp greenpepper-core.jar:greenpepper-extensions-java.jar com.greenpepper.runner.Main \
         -d com.greenpepper.runner.dialect.MarkdownDialect \
         specification.md \
         output.html
```

## Differences between the *GreenPepper Markdown* syntax and *CommonMark*

[CommonMark] is a specification defining a strict markdown syntax. Even though the
initiative is needed, the specification becomes hightly restrictive for usage in software.

Greenpepper extends this specification for handling **html tables** in a more flexible way.

### Allowed differences between column numbers
 
Greenpepper allows the following :


```
| do with | Banker |
|---------|--------|
| accept | open an account for | John |
| display | the id of | John | account |
| check  | the balance of | John | account is | 0 |
```
 
### Multiple lines headers

Greenpepper allows the following :

```
| list of | Accounts |
| owner | balance | id  |
|-------|---------|-----|
| John  | 0       | abc |
```
 
