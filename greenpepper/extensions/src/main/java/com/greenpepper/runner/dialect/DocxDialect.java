package com.greenpepper.runner.dialect;

import com.greenpepper.dialect.SpecificationDialect;
import com.greenpepper.dialect.SpecificationDialectException;
import fr.opensagres.xdocreport.converter.*;
import fr.opensagres.xdocreport.core.document.DocumentKind;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class DocxDialect extends AbstractOpenDocumentDialect {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocxDialect.class);

    @Override
    public String convert(InputStream input) throws SpecificationDialectException {
        LOGGER.debug("Converting content to XHTML");
        // 1) Create options ODT 2 PDF to select well converter form the registry
        Options options = Options.getFrom(DocumentKind.DOCX).to(ConverterTypeTo.XHTML);

        XHTMLOptions xhtmlOptions = XHTMLOptions.create();
        xhtmlOptions.indent(2);
        xhtmlOptions.setIgnoreStylesIfUnused(true);
        options.subOptions(xhtmlOptions);

        // 2) Get the converter from the registry
        IConverter converter = ConverterRegistry.getRegistry().getConverter(options);

        // 3) Convert ODT 2 PDF
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            converter.convert(input, out, options);
            out.flush();
            String result = cleanUpTheHtml(out.toString());
            LOGGER.debug("result of the conversion:\n{}", result);
            return result;
        } catch (XDocConverterException e) {
            throw new SpecificationDialectException("unable to convert to XHTML", e);
        } catch (IOException e) {
            throw new SpecificationDialectException("unable to convert to XHTML", e);
        }
    }

}
