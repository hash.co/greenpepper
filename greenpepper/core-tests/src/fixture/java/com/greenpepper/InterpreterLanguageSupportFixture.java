package com.greenpepper;

import com.greenpepper.annotation.Fixture;
import com.greenpepper.annotation.FixtureMethod;
import com.greenpepper.reflect.Type;

import java.util.Locale;

@Fixture(usage = "Interpreter language support")
public class InterpreterLanguageSupportFixture {

	public String interpreterName;
	public String locale;

	@FixtureMethod(usage = "interpreter class ")
	public String interpreterClass() {
		GreenPepper.setLocale(new Locale(locale));
		Type<Interpreter> interpreterType = GreenPepper.interpreterTypeLoader().loadType(interpreterName);

		if (interpreterType != null && interpreterType.getUnderlyingClass() != null)
			return interpreterType.getUnderlyingClass().getSimpleName();
		return null;
	}
}