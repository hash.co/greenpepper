# Language support

| import |
|--------|
| com.greenpepper |
| com.greenpepper.interpreter.flow.dowith |

## Interpreter

### EN

| rule for | Interpreter language support ||
|----------|------------------------------|-------------|
| **Locale** | **interpreter name** | **interpreter class ?** |
| en | Do with | DoWithInterpreter |
| en | Setup | SetupInterpreter |
| en | Scenario | ScenarioInterpreter |
| en | List of | ListOfInterpreter |
| en | Set of | SetOfInterpreter |
| en | Subset of | SubsetOfInterpreter |
| en | Superset of  | SupersetOfInterpreter |
| en | unknown | null |

### FR

| rule for | Interpreter language support ||
|----------|------------------------------|-------------|
| **Locale** | **interpreter name** | **interpreter class ?** |
| fr | Utiliser | DoWithInterpreter |
| fr | préparer | SetupInterpreter |
| fr | Scénario | ScenarioInterpreter |
| fr | Liste de | ListOfInterpreter |
| fr | Ensemble de | SetOfInterpreter |
| fr | Sous-ensemble de | SubsetOfInterpreter |
| fr | Sur-ensemble de | SupersetOfInterpreter |
| fr | unknown | null |


### FakeLang

| rule for | Interpreter language support ||
|----------|------------------------------|-------------|
| **Locale** | **interpreter name** | **interpreter class ?** |
| FakeLang | Do with | DoWithInterpreter |
| FakeLang | Setup | SetupInterpreter |
| FakeLang | Scenario | ScenarioInterpreter |
| FakeLang | List of | ListOfInterpreter |
| FakeLang | Set of | SetOfInterpreter |
| FakeLang | Subset of | SubsetOfInterpreter |
| FakeLang | Superset of  | SupersetOfInterpreter |
| FakeLang | unknown | null |

## Do with action

### EN

| rule for | Dowith action language support ||
|----------|------------------------------|-------------|
| **Locale** | **interpreter name** | **interpreter class ?** |
| en | accept | AcceptRow |
| en | check | CheckRow |
| en | display | DisplayRow |
| en | reject | RejectRow |
| en | skip | SkipRow |
| en | end | EndRow |
| en | unknown | null |

### FR

| rule for | Dowith action language support ||
|----------|------------------------------|-------------|
| **Locale** | **interpreter name** | **interpreter class ?** |
| fr | accepter | AcceptRow |
| fr | vérifier | CheckRow |
| fr | afficher | DisplayRow |
| fr | rejetter | RejectRow |
| fr | passer | SkipRow |
| fr | fin | EndRow |
| fr | unknown | null |

### FakeLank

| rule for | Dowith action language support ||
|----------|------------------------------|-------------|
| **Locale** | **interpreter name** | **interpreter class ?** |
| FakeLang | accept | AcceptRow |
| FakeLang | check | CheckRow |
| FakeLang | display | DisplayRow |
| FakeLang | reject | RejectRow |
| FakeLang | skip | SkipRow |
| FakeLang | end | EndRow |
| FakeLang | unknown | null |


