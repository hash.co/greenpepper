// Gulp build
var gulp = require('gulp');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var imagemin = require('gulp-imagemin');
var del = require('del');
var cache = require('gulp-cache');
var runSequence = require('run-sequence');
var bower = require('gulp-bower');
var rev = require('gulp-rev');
var revAppend = require('gulp-rev-append');
var revReplace = require('gulp-rev-replace');
var karma = require('karma');

gulp.task('karma-test', function(done){
    new karma.Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
});


gulp.task('bower', function () {
    return bower('src/main/webapp/bower_components');
});

gulp.task('useref', function () {
    return gulp.src(['src/main/webapp/**/*.html',
        '!src/main/webapp/bower_components/**/*'])
        .pipe(useref())
        // Minifies only if it's a JavaScript file
        .pipe(gulpIf('*.js', uglify()))
        // Minifies only if it's a CSS file
        .pipe(gulpIf('*.css', cssnano()))
        .pipe(gulpIf('!**/*.html',rev()))
        .pipe(gulpIf('**/*.html', revAppend()))
        .pipe(revReplace())
        .pipe(gulp.dest('target/dist'))
});

gulp.task('images', function () {
    return gulp.src('src/main/webapp/support/image/**/*.+(png|jpg|gif|svg|ico)')
    // optimizing images
        .pipe(cache(imagemin({
            // Setting interlaced to true
            interlaced: true
        })))
        .pipe(gulp.dest('target/dist/support/image'))
});

gulp.task('fonts', function () {
    return gulp.src([
        'src/main/webapp/support/fonts/**/*',
        'src/main/webapp/bower_components/bootstrap/dist/fonts/**/*'])
        .pipe(gulp.dest('target/dist/support/fonts'))
});

gulp.task('clean:dist', function () {
    return del.sync('target/dist');
});

gulp.task('cache:clear', function (callback) {
    return cache.clearAll(callback)
});


gulp.task('test', function (callback) {
    runSequence('bower', 'karma-test', callback);
});

gulp.task('build', function (callback) {
    runSequence('clean:dist', 'bower', ['useref', 'images', 'fonts'], callback)
});

gulp.task('default', function (callback) {
    runSequence('bower', ['useref', 'images', 'fonts'], callback)
});

