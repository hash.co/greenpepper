describe("LocationService", function () {
    beforeEach(module('greenpepper.services'));

    var $controller;
    var $log;
    var service;

    beforeEach(inject(function (LocationService, _$controller_, _$log_) {
        service = LocationService;
        $controller = _$controller_;
        $log = _$log_;
    }));


    // Log debug messages in Karma
    /*  afterEach(function () {
     console.log($log.debug.logs);
     console.log($log.info.logs);
     console.log($log.warn.logs);
     console.log($log.error.logs);
     console.log($log.log.logs);
     });
     */
    it("should add count to the location", function () {

        inject(function ($location) {
            service.addCount(45);
            expect($location.url()).toBe("?count=45");
            service.addCount();
            expect($location.url()).toBe("");
        });
    });

    it("should add search to the location", function () {

        inject(function ($location) {
            service.addSearch("My spec");
            expect($location.url()).toBe("?search=My%20spec");
            service.addSearch();
            expect($location.url()).toBe("");
        });
    });

    it("should add state to the location", function () {

        inject(function ($location) {
            service.addResultState("run");
            expect($location.url()).toBe("?state=run");
            service.addResultState("any");
            expect($location.url()).toBe("?state=any");
            service.addResultState();
            expect($location.url()).toBe("");
        });
    });

    it("should add trust to the location", function () {

        inject(function ($location) {
            service.addTrust(true);
            expect($location.url()).toBe("?trusted=true");
            service.addTrust(false);
            expect($location.url()).toBe("?trusted=false");
            service.addTrust();
            expect($location.url()).toBe("");
        });
    });

    it("should add Repo to the location", function () {

        inject(function ($location) {
            service.addRepo("Repo1");
            expect($location.url()).toBe("?repo=Repo1");
            service.addRepo();
            expect($location.url()).toBe("");

            var a;
            service.addRepo(a);
            expect($location.url()).toBe("");
        });
    });
});

