(function() {
    "use strict";

    angular.module('greenpepper.report').controller('ResultsController', ResultsController);

    ResultsController.$inject = ['NgTableParams', '$uibModal', 'FileLoader', 'ResultPage', "LocationService", '$scope', '$filter',
        '$location', 'ngTableEventsChannel'];

    function ResultsController(NgTableParams, $uibModal, FileLoader, ResultPage, LocationService, $scope, $filter,
                               $location, ngTableEventsChannel) {

        $scope.statefilter = "all";
        $scope.tests = [];
        $scope.report = {};
        $scope.projects = [];
        $scope.currentProject = {};
        $scope.initFailure = null;
        $scope.loadingIndex = false;
        $scope.indexLoaded = false;

        $scope.loadProjects = loadProjects;
        $scope.loadIndexes = loadIndexes;
        $scope.filterByState = filterByState;
        $scope.displayHTMLResult = displayHTMLResult;
        $scope.displayResultLog = displayResultLog;

        $scope.tableParams = new NgTableParams({sorting: {name: "asc"}});
        var lastStateParams = LocationService.getParams();

        ngTableEventsChannel.onAfterReloadData(function(){
            // reload data: this is due to a change of state in the table.
            LocationService.addCount($scope.tableParams.count());

            var filter = $scope.tableParams.filter();
            if (angular.isUndefined(filter.name)) {
                LocationService.addSearch();
            } else {
                LocationService.addSearch(filter.name);
            }
            if (angular.isUndefined(filter.implemented)) {
                LocationService.addTrust();
            } else {
                LocationService.addTrust(filter.implemented);
            }

        });

        function applyPreviousState() {
            console.log("apply previous state");
            console.log(lastStateParams);

            if (angular.isDefined(lastStateParams.count)) {
                $scope.tableParams.count(lastStateParams.count);
            }
            var filter = $scope.tableParams.filter();
            if (angular.isDefined(lastStateParams.state)) {
                $scope.statefilter = lastStateParams.state;
                _getFilterByState(filter);
            }
            if (angular.isDefined(lastStateParams.search)) {
                filter['name'] = lastStateParams.search;
            }
            if (angular.isDefined(lastStateParams.trusted)) {
                if (lastStateParams.trusted === 'true') {
                    filter['implemented'] = true;
                } else if (lastStateParams.trusted === 'false') {
                    filter['implemented'] = false;
                }
            }
            $scope.tableParams.filter(filter);
        }

        function loadProjects() {

            FileLoader.loadreports().then(loadreportsInPage, _displayInitError);

            function loadreportsInPage(response) {
                var results = [];
                var indexCorrupted = false;
                $.each(response.data, function (name, project) {
                    if (project && angular.isObject(project)) {
                        results.push(project);
                    } else {
                        indexCorrupted = true;
                    }
                });

                if (indexCorrupted) {
                    _displayInitErrorCustom("Corrupted index.json file.",
                        "The <b>index.json</b> file has been found but is not a list.<br/>" +
                        "It should be a list of project description.");
                } else {
                    $scope.projects = results;
                    if (results.length > 0) {
                        if (angular.isDefined(lastStateParams.repo)) {
                            for (var i=0 ; i< results.length; i++) {
                                if (lastStateParams.repo === results[i].repoName) {
                                    $scope.currentProject = results[i];
                                    break;
                                }
                            }
                        }
                        if (!$scope.currentProject.repoName) {
                            $scope.currentProject = results[0];
                        }
                        loadIndexes($scope.currentProject.repoId);
                    } else {
                        _displayInitErrorCustom("No project to display",
                            "The list for project to display the result for is empty.<br/>" +
                            "Did you run your specification tests?");
                    }
                }
            }
        }

        function loadIndexes(repoId) {
            $scope.loadingIndex = true;
            $scope.indexLoaded = false;
            $scope.initFailure = null;
            $scope.statefilter = "all";

            function loadResults() {
                FileLoader.getresults(repoId).then(function (results) {
                    $scope.report.resultsfound = true;
                    $scope.report.specsok = 0;
                    $scope.report.specsko = 0;
                    $scope.report.specsskipped = 0;
                    $scope.report.specsnotrun = 0;
                    $.each($scope.tests, function (i, test) {
                        var testResult = results.data[test.name];
                        if (testResult) {
                            test.success = testResult.statistics.rightCount;
                            test.failure = testResult.statistics.wrongCount;
                            test.error = testResult.statistics.exceptionCount;
                            test.duration = testResult.duration / 1000;
                            test.logs = FileLoader.getlogsurl($scope.currentProject.repoName, test.name);
                            if (test.failure > 0 || test.error > 0) {
                                test.result = "failed";
                                $scope.report.specsko++;
                            } else if (test.success > 0) {
                                test.result = "succeeded";
                                $scope.report.specsok++;
                            } else {
                                test.result = "skipped";
                                $scope.report.specsskipped++;
                            }
                        } else {
                            test.result = "notrun";
                            $scope.report.specsnotrun++;
                        }
                    });
                    $scope.tableParams.settings({
                        total: $scope.tests.length,
                        dataset: $scope.tests
                    });
                    $scope.report.buildchart.data = [$scope.report.specsok, $scope.report.specsko, $scope.report.specsskipped, $scope.report.specsnotrun];

                    applyPreviousState();
                });
            }

            if (bowser.chrome && $location.protocol() === 'file') {
                _displayInitErrorCustom("Incompatible usage with chrome/chromium browser .",
                    "<p>Opening the html page from the File system, in Chrome is not supported. Choose one of the following solutions: <ul>" +
                    "<li>Use another browser: Firefox for instance</li>" +
                    "<li>Use your IDE to launch it via its internal webserver.</li>" +
                    "<li>Use python to create a local webserver via <code>python -m SimpleHTTPServer</code> in the reports folder.</li>" +
                    "</ul></p>",
                    "https://greenpepper.atlassian.net/wiki/display/DOC/Launching+the+index+file+from+Chrome"
                );
            } else {
                var customComparatorWithChoicesAsArray = function(value, searchTerm) {
                    if (angular.isDefined(searchTerm) && searchTerm !== '') {
                        if (angular.isArray(searchTerm) && !angular.isArray(value)) {
                            return searchTerm.indexOf(value) > -1;
                        } else if (angular.isString(value) && angular.isString(searchTerm)) {
                            return (value || '').toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
                        } else {
                            return angular.equals(value, searchTerm);
                        }
                    }
                    // means no filter
                    return true;

                };
                $scope.tableParams = new NgTableParams({
                        // initial filter
                        sorting: {name: "asc"}
                    },
                    { counts: [50, 100, 500, 1000] },
                    { getData: function (params) {
                            var sortedData = params.sorting() ? $filter('orderBy')($scope.tests, params.orderBy()) : $scope.tests;
                            var filteredData = params.filter() ? $filter('filter')(sortedData, params.filter(), customComparatorWithChoicesAsArray) : sortedData;
                            // the total of pages is the filtered one
                            params.total(filteredData.length);
                            $scope.report.displaynumber = filteredData.length;
                            // Now the current page
                            return filteredData.slice((params.page() - 1) * params.count(), params.page() * params.count());
                        },
                        filterDelay: 200
                    });

                FileLoader.getindex(repoId).then(function (specs) {
                    $scope.tests = [];
                    var i = 0;
                    var nbCertified = 0;
                    $.each(specs.data, function (name, spec) {
                        var test = {"id": i, "name": name, "implemented" : spec.implemented};
                        if (spec.link) {
                            test.link = spec.link;
                        }
                        if (spec.implemented) {
                            nbCertified ++;
                        }
                        $scope.tests.push(test);
                        i++;
                    });
                    $scope.report.certifiedspecs = nbCertified;
                    $scope.report.specstotal = $scope.tests.length;
                    $scope.tableParams.settings({
                        total: $scope.tests.length,
                        dataset: $scope.tests
                    });
                    $scope.report.buildchart = {
                        data: [0, 0, 0, $scope.report.specstotal],
                        labels: ["Succeeded", "Failing", "Skipped", "Not run"],
                        colors: ["#3c763d", "#a94442", "#8a6d3b", "#737373"]
                    };
                    $scope.report.trustchart = {
                        data: [$scope.report.certifiedspecs, $scope.report.specstotal - nbCertified],
                        labels: ["Implemented", "Not Implemented"],
                        colors: ["#3c763d", "#8a6d3b"]
                    };

                    loadResults();

                    $scope.loadingIndex = false;
                    $scope.indexLoaded = true;

                    LocationService.addRepo($scope.currentProject.repoName);

                }, _displayInitError);
            }
        }

        function _getFilterByState(filter) {
            if ($scope.statefilter === 'all') {
                delete filter["result"];
                LocationService.addResultState();
            } else if ($scope.statefilter === 'run') {
                filter["result"] = ['succeeded', 'failed', 'skipped'];
                LocationService.addResultState("run");
            } else {
                filter["result"] = $scope.statefilter;
                LocationService.addResultState($scope.statefilter);
            }
        }

        function filterByState() {
            var filter = $scope.tableParams.filter();
            _getFilterByState(filter);
            $scope.tableParams.filter(filter);
        }

        function displayHTMLResult(repoName, specId) {

            function displayResponse(response) {
                var page = response.data;
                var viewspec = $("#view-spec-" + specId);
                if (viewspec.length === 0) {
                    $("#spec-" + specId).after("<tr id='result-viewer-" + specId +
                        "' class='result-viewer'><td colspan='8'><div id='view-spec-" + specId + "'></div></td></tr>");
                }
                viewspec = $("#view-spec-" + specId);
                viewspec.html(page);
                ResultPage.badgePageAsync(viewspec);
                $("#result-viewer-" + specId).show();
            }

            if ($("#result-viewer-" + specId).is(':visible')) {
                $("#result-viewer-" + specId).hide();
            } else {
                $(".result-viewer").hide();
                var specName = $scope.tests[specId].name;
                FileLoader.getpage(repoName, specName).then(displayResponse, displayResponse);
            }
        }

        function displayResultLog(repoName, specId, event) {
            event.preventDefault();
            event.stopPropagation();
            var specName = $scope.tests[specId].name;
            FileLoader.getlog(repoName, specName).then(openLogsModalSuccess, openLogsModalError);

            function openLogsModalSuccess (response) {
                $scope.logs = response.data;
                $uibModal.open({
                    templateUrl: 'support/app/report/logsmodal.html',
                    size: 'lg',
                    controller: 'LogsModalController',
                    resolve: {
                        logs: function () {
                            return { text: $scope.logs };
                        }
                    },
                    windowClass: 'logs-modal-window'
                });
            }

            function openLogsModalError (response) {
                $uibModal.open({
                    templateUrl: 'support/app/report/logsmodal.html',
                    size: 'lg',
                    controller: 'LogsModalController',
                    resolve: {
                        logs: function () {
                            return { errorMessage: response.statusText, text: response.data };
                        }
                    },
                    windowClass: 'logs-modal-window'
                });
            }

        }

        function _displayInitError(response) {
            $scope.loadingIndex = false;
            $scope.indexLoaded = false;
            _displayInitErrorCustom("Unable to load the index file.",
                response.data ? response.data : response.statusText
            );
        }

        function _displayInitErrorCustom(reason, details, moreinfo) {
            $scope.initFailure = {
                reason: reason,
                details: details,
                moreinfo: moreinfo
            };
        }
    }

})();
