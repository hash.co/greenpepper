(function() {
    "use strict";

    angular.module('greenpepper.report').controller('LogsModalController', LogsModalController);

    LogsModalController.$inject = ['$scope', 'logs'];

    function LogsModalController($scope, logs) {
        $scope.logs = logs;
    }
})();