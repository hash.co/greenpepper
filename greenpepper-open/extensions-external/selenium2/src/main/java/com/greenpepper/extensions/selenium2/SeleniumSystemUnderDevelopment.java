
/**
 * Copyright (c) 2010 Pyxis Technologies inc.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA,
 * or see the FSF site: http://www.fsf.org.
 *
 * @author oaouattara
 * @version $Id: $Id
 */
package com.greenpepper.extensions.selenium2;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.greenpepper.document.Document;
import com.greenpepper.reflect.DefaultFixture;
import com.greenpepper.reflect.Fixture;
import com.greenpepper.systemunderdevelopment.DefaultSystemUnderDevelopment;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.util.Arrays;

import static org.apache.commons.lang3.StringUtils.equalsIgnoreCase;

public class SeleniumSystemUnderDevelopment
		extends DefaultSystemUnderDevelopment {

	private Injector injector;
	private WebDriver driver;

    /**
     * Default constructor sets HTMLUnitDriver
     */
    public SeleniumSystemUnderDevelopment() {
        this("htmlunit");
    }

	/**
	 * Constructor that allows to set a webdriver by configuration.
	 *
	 * @param args sets a webdriver as a string. Supported values are: <ul>
	 *             <li>firefox</li>
	 *             <li>chrome</li>
	 *             <li>safari</li>
	 *             <li>htmlunit (default)</li>
	 * </ul>
	 */
	public SeleniumSystemUnderDevelopment(String... args) {
		this(getWebDriverModule(args));
		if (args.length >= 2) {
			String browser = args[0];
			boolean downloadWebDriver = Boolean.parseBoolean(args[1]);
			if (downloadWebDriver) {
				WebDriverManager webDriverManager;
				if (equalsIgnoreCase("firefox", browser)) {
					webDriverManager = WebDriverManager.firefoxdriver();
				} else if (equalsIgnoreCase("chrome", browser)) {
					webDriverManager = WebDriverManager.chromedriver();
				} else if (equalsIgnoreCase("opera", browser)) {
					webDriverManager = WebDriverManager.operadriver();
				} else if (equalsIgnoreCase("edge", browser)) {
					webDriverManager = WebDriverManager.edgedriver();
				} else if (equalsIgnoreCase("ie", browser)) {
					webDriverManager = WebDriverManager.iedriver();
				} else {
					throw new IllegalArgumentException(String.format("%s is not a supported browser by webdrivermanager", browser));
				}
				if (args.length >= 3) {
					String webdriverVersion = args[2];
					webDriverManager.version(webdriverVersion);
				}
				webDriverManager.setup();
			}
		}
	}

	private static WebDriverModule getWebDriverModule(String[] args) {
		WebDriverModule webDriverModule;
		if (args.length == 0) {
			webDriverModule = new WebDriverModule("htmlunit", null);
		} else {
			String browser = args[0];
			String properties = null;
			if (args.length >= 4) {
				properties = args[3];
			}
			webDriverModule = new WebDriverModule(browser, properties);
		}
		return webDriverModule;
	}

	/**
	 * <p>Constructor for SeleniumSystemUnderDevelopment.</p>
	 *
	 * @param modules a {@link com.google.inject.Module} object.
	 */
	public SeleniumSystemUnderDevelopment(Module... modules) {
		injectModules(modules);
	}

	private void injectModules(Module[] modules) {
		injector = Guice.createInjector(modules);
	}

	/** {@inheritDoc} */
	@Override
	public void onStartDocument(Document document) {
		driver = injector.getInstance(WebDriver.class);
	}

	/** {@inheritDoc} */
	@Override
	public void onEndDocument(Document document) {
		if (driver != null) {
			driver.quit();
		}
	}

	/** {@inheritDoc} */
	@Override
	public Fixture getFixture(String name, String... params) {

		Class<?> klass = loadType(name).getUnderlyingClass();
		Object target = injector.getInstance(klass);
		return new DefaultFixture(target);
	}
}
