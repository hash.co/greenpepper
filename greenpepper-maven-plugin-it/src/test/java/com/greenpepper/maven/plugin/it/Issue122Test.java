package com.greenpepper.maven.plugin.it;


import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.ResourceExtractor;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;

import static com.greenpepper.maven.plugin.it.GreenPepperMavenPluginTest.testLaunchingMaven;
import static java.lang.String.format;
import static org.apache.maven.it.util.FileUtils.fileRead;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class Issue122Test {

    @Test
    public void shouldNotGetANullPointerException() throws Exception {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/issue122");
        Verifier verifier = testLaunchingMaven(testDir, new ArrayList<String>(), "integration-test");

        verifier.verifyErrorFreeLog();
        String logFile = format("%s/%s", verifier.getBasedir(), verifier.getLogFileName());
        assertThat("should not get a NPE", fileRead(logFile), not(containsString("NullPointerException")));
    }

}
