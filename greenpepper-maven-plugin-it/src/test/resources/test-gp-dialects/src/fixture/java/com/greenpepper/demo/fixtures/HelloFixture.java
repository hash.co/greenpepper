package com.greenpepper.demo.fixtures;

import com.greenpepper.annotation.Fixture;
import com.greenpepper.annotation.FixtureMethod;

@Fixture(usage = "Hello")
public class HelloFixture {

	private String person;

	@FixtureMethod(usage = "meet the person named")
	public boolean meetThePersonNamed(String param1) {
		this.person = param1;
		return true;
	}

	@FixtureMethod(usage = "that Hello says")
	public String thatHelloSays() {
		return "hello " + person + " !";
	}
}