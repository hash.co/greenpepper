package com.greenpepper.demo.fixtures;

import com.greenpepper.annotation.Fixture;
import com.greenpepper.annotation.FixtureMethod;

@Fixture(usage = "Basic Calculator")
public class BasicCalculatorFixture {

	public Double x;
	public Double y;

	@FixtureMethod(usage = "Divide ")
	public Double divide() {
		if (y == 0)
			throw new IllegalArgumentException("divide by 0");
		return x / y;
	}

	@FixtureMethod(usage = "Multiply ")
	public Double multiply() {
		return x * y;
	}

	@FixtureMethod(usage = "Sum ")
	public Double sum() {
		return x + y;
	}
}