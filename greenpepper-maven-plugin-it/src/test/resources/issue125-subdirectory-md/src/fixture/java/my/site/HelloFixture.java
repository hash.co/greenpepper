package my.site;

import com.greenpepper.annotation.Fixture;
import com.greenpepper.annotation.FixtureMethod;

import static java.lang.String.format;

@Fixture(usage = "Hello")
public class HelloFixture {

	String person;

	@FixtureMethod(usage = "meet the person named")
	public boolean meetThePersonNamed(String param1) {
		this.person = param1;
		return true;
	}

	@FixtureMethod(usage = "that Hello says")
	public String thatHelloSays() {
		return format("Hello %s !", person);
	}
}