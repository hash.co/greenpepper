package com.greenpepper.maven.plugin;

import com.greenpepper.document.Document;
import com.greenpepper.systemunderdevelopment.DefaultSystemUnderDevelopment;


public class CustomSUD extends DefaultSystemUnderDevelopment {

    private String[] args;

    public CustomSUD(String... args) {
        this.args = args;
    }

    @Override
    public void onStartDocument(Document document) {
        for (int i = 0; i < args.length; i++) {
            System.out.println(String.format("Argument %d is: %s", i, args[i]));
        }
    }
}
