package com.greenpepper.maven.plugin.utils;

import com.greenpepper.Statistics;

public interface StatisticsListener {

    /**
     * Notify a listener of the completion of the specification testing.
     *  @param name the name of the specification
     * @param stats the results of the test
     * @param duration the duration of the test.
     */
    void notify(String name, Statistics stats, long duration);
}
