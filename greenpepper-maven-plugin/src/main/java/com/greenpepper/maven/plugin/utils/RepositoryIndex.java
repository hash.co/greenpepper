package com.greenpepper.maven.plugin.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;

public class RepositoryIndex extends AbstractIndex<RepositoryIndex.SpecificationInfo> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryIndex.class);

    public RepositoryIndex(File indexFile) throws IOException {
        super(indexFile);
    }

    @Override
    protected TypeReference<LinkedHashMap<String, SpecificationInfo>> getValueTypeRef() {
        return new TypeReference<LinkedHashMap<String, SpecificationInfo>>() {};
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    public static class SpecificationInfo {
        boolean implemented;
        String link;
        public void setImplemented(boolean implemented) {
            this.implemented = implemented;
        }


        public boolean isImplemented() {
            return implemented;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public String getLink() {
            return link;
        }
    }
}
