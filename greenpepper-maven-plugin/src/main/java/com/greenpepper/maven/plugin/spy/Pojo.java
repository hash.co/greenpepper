package com.greenpepper.maven.plugin.spy;

import com.greenpepper.spy.PojoDescription;
import com.greenpepper.spy.PropertyDescription;
import org.apache.commons.lang3.StringUtils;

import java.util.Collection;

public class Pojo extends Spy<Pojo> implements PojoDescription {

    private final Collection<PropertyDescription> properties;

    public Pojo(String rawName, Collection<PropertyDescription> properties) {
        super(rawName);
        this.properties = properties;
    }

    @Override
    public Collection<PropertyDescription> getProperties() {
        return properties;
    }

    @Override
    public String getName() {
        return StringUtils.capitalize(super.getName());
    }
}
