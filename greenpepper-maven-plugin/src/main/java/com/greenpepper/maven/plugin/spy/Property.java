package com.greenpepper.maven.plugin.spy;

import com.greenpepper.spy.PropertyDescription;

public class Property extends Spy<Property> implements PropertyDescription {

    Property(String rawName) {
        super(rawName);
    }

    public boolean equals(Object o) {
        return o != null
                && o instanceof Property
                && this.getName().equals(((Property) o).getName());
    }

}

