package com.greenpepper.maven.plugin.runner;

import com.greenpepper.maven.plugin.TestMonitor;
import com.greenpepper.maven.plugin.utils.NullStatisticsListener;
import com.greenpepper.maven.plugin.utils.StatisticsListener;
import com.greenpepper.runner.CompositeSpecificationRunnerMonitor;
import com.greenpepper.runner.RecorderMonitor;
import com.greenpepper.server.domain.Specification;
import com.greenpepper.server.domain.SystemUnderTest;
import org.apache.maven.plugin.logging.Log;

import java.io.IOException;

import static java.lang.String.format;

public class RunnerTask implements Runnable {

    private final Log logger;
    private final RecorderMonitor recorder = new RecorderMonitor();

    private final Specification specification;
    private final SystemUnderTest systemUnderTest;
    private final String outputPath;
    private final Runner runner;

    private final StatisticsListener statisticsListener;

    public RunnerTask(Specification specification, SystemUnderTest systemUnderTest, String outputPath, Runner runner, Log logger, StatisticsListener statisticsListener) {
        this.specification = specification;
        this.systemUnderTest = systemUnderTest;
        this.outputPath = outputPath;
        this.runner = runner;
        this.logger = logger;
        if (statisticsListener != null) {
            this.statisticsListener = statisticsListener;
        } else {
            this.statisticsListener = new NullStatisticsListener();
        }
    }

    @Override
    public void run() {
        try {
            CompositeSpecificationRunnerMonitor monitors = new CompositeSpecificationRunnerMonitor();
            TestMonitor forkMonitor = new TestMonitor(logger, statisticsListener);
            monitors.add(forkMonitor);
            monitors.add(recorder);
            runner.execute(specification, systemUnderTest, outputPath, monitors);
        } catch (IOException e) {
            logger.error(format("Error running specification %s", specification), e);
        }
    }

    public RecorderMonitor getRecorder() {
        return recorder;
    }

    public Specification getSpecification() {
        return specification;
    }

}
