package com.greenpepper.maven.plugin;

import java.util.HashMap;
import java.util.Map;

public class FixtureGeneratorDefinition {

    String impl;

    private Map<String, String> properties = new HashMap<String, String>();

    String getImpl() {
        return impl;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(impl).append('\n');
        for (Map.Entry<String, String> entry : properties.entrySet()) {
            stringBuilder.append("  - ").append(entry.getKey()).append(": ").append(entry.getValue()).append('\n');
        }
        return stringBuilder.toString();
    }
}
