# Repository configuration

The repository configuration is meant to specify where to get/download the specification from.

```xml
<repository>
    <name/>
    <isDefault/>
    <projectName/>
    <systemUnderTest/>
    <runnerName/>
    <type/>
    <root/>
    <dialect/>
    <suites>
        <suite/>
    </suites>
    <tests>
        <test/>
    </tests>
</repository>
```

## Nodes details 

### repository

| Element  | Description | Type |
|---|---|---|
|  **name** | | String |
|  **isDefault** | | boolean |
|  **projectName** | | String |
|  **systemUnderTest** | | String |
|  **runnerName** | The [runner](./runner-ref.html) assigned to this repository. It defaults to the default JAVA runner if not set. | String |
|  **type** | The default value is `com.greenpepper.repository.FileSystemRepository` | Class name |
|  **root** | | String |
| **dialect** | The dialect to use with this repository. It's a Class name, eventually followed by the constructor args separated by semicolon.| Class;Args1;Args2 |
|  **suites** | | List of `<suite>` . Each **suite** is a string. |
|  **tests** | | List of `<test>` . Each **test** is a string. |
