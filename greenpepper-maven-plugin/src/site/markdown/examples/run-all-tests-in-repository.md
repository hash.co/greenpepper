The greenepper core tests configuration contains something like the bellow configuration.   
During the `integration-test` phase, we compile the fixtures and we run all the tests found in `src/specs/greenpepper`.


```xml
<plugin>
    <groupId>com.github.strator-dev.greenpepper</groupId>
    <artifactId>greenpepper-maven-plugin</artifactId>
    <version>${project.version}</version>
    <configuration>
        <fixtureSourceDirectory>src/fixture/java</fixtureSourceDirectory>
        <fixtureOutputDirectory>target/test-classes</fixtureOutputDirectory>
        <systemUnderDevelopment>com.greenpepper.systemunderdevelopment.GreenPepperSystemUnderDevelopment</systemUnderDevelopment>

        <repositories>
            <repository>
                <type>com.greenpepper.repository.FileSystemRepository</type>
                <root>${basedir}/src/specs/greenpepper</root>
                <name>GreenPepper-specifications</name>
            </repository>
        </repositories>
    </configuration>
    <executions>
        <execution>
            <id>makeall</id>
            <phase>integration-test</phase>
            <goals>
                <goal>compile</goal>
                <goal>run</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

In the case of a **FileSystemRepository**, such a configuration makes greenpepper discover all the `.html` files in the `root` folder.

> **Info:**
>
> The resulting page can be viewed [here](http://strator-dev.github.io/greenpepper/core-tests/greenpepper-reports/GreenPepper-specifications.html).
 
