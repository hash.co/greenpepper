# Using multiple runners to launch your specifications tests

When using the maven plugin, you usually want to test a java application that is accessible in
your project. This is the most basic case.  
Now suppose that you want to take advantage of a launch of the plugin to also test your __C#__ 
part of your application. You will be able to do it using the **Runners**.

## What is a Runner

A **Runner** is a command that can be defined to launch a specification test.  
Testing a Java application will probably be something like 

```bash
java -cp <my_classpath>:greenpepper-core.jar spec.html
```

> **Note**: It's actually more complicated but I put this for the sake of simplicity.

For __C#__ you will like something like:

```bash
GreenPepper.exe -a fixtures.dll;system-under-test.dll;external-dependency.dll spec.html
```

In the maven plugin the two lines will be defined in 2 *Runners*.

## Example of Runners configuration

In the below configuration, we will associate runners to specification repositories:
  * **JAVA Spec** repository has the runner **java**
  * **C# Spec** repository has the runner **csharp**
  * **Spec** repository has no runner specified. It will be managed by the default runner.


```xml
    <repositories>
        <repository>
            <type>com.greenpepper.repository.FileSystemRepository</type>
            <root>../specs/default</root>
            <name>Spec</name>
            <projectName>PROJECT</projectName>
            <systemUnderTest>SUT</systemUnderTest>
        </repository>
        <repository>
            <type>com.greenpepper.repository.FileSystemRepository</type>
            <root>../specs/java</root>
            <name>JAVA Spec</name>
            <projectName>PROJECT</projectName>
            <systemUnderTest>SUT</systemUnderTest>
            <runnerName>java</runnerName>
        </repository>
        <repository>
            <type>com.greenpepper.repository.FileSystemRepository</type>
            <root>../specs/csharp</root>
            <name>C# Spec</name>
            <projectName>PROJECT</projectName>
            <systemUnderTest>SUT</systemUnderTest>
            <runnerName>csharp</runnerName>
        </repository>
    </repositories>

    <runners>
        <runner>
            <name>java</name>
            <forkCount>2</forkCount>
            <cmdLineTemplate>java -cp {classpaths} {mainClass} {inputPath} {outputPath} -r {repository} -f {fixtureFactory}</cmdLineTemplate>
            <mainClass>com.greenpepper.runner.Main</mainClass>
            <includeProjectClasspath>true</includeProjectClasspath>
        </runner>
        <runner>
            <name>csharp</name>
            <forkCount>2</forkCount>
            <cmdLineTemplate>GreenPepper.exe -a {classpaths} {inputPath} {outputPath}</cmdLineTemplate>
            <includeProjectClasspath>true</includeProjectClasspath>
        </runner>
    </runners>
```

### Default Runner

In the configuration example shown above, the repository named **Spec** will have its specifications launched by the Default Runner which is 
the JAVA runner embedded in the greenpepper maven plugin. See the [Runner] page for more information on the default runner.

> **tips**: You can exclude the default runner if you are in a context in which you don't need it. Use the **excludeDefaultRunner** parameter for that purpose.

