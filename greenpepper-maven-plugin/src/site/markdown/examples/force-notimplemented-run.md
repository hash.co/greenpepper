
The default behaviour is to run only the implemented specifications. 
It is possible however to set your run up to launch the last version of the specification (Implemented or not).

> **tip:** Best practice
> 
> We don't recommend usage of this mode to assert the quality of your software. You may use it on your development 
> platform, to implement your specifications.

The setting can be made under the repository and is specific to each type of repository.

## Settings on Atlassian repository

Using the `root` setting of the repository. This setting is splitted using `;` and passed to the repository `type` class constructor.

For example.

```
http://localhost:19005/rpc/xmlrpc?handler=greenpepper1#SPACE KEY;login;password;true
```

| Parameter position | Description |
|------------------|--------------------------------------------------------------------------------------|
| **http://localhost:19005/rpc/xmlrpc?handler=greenpepper1#SPACE KEY** | The Atlassian repository entry point |
| **login**          | the login for authentication                                                           |
| **password**       | the password for authentication                                                        |
| **true**           | Should the plugin run the Not implemented version of the page. Default **false**       |



### If the repository requires an authentication

```xml
<plugins>
    <plugin>
        <groupId>com.github.strator-dev.greenpepper</groupId>
        <artifactId>greenpepper-maven-plugin</artifactId>
        <version>${project.version}</version>
        <configuration>
            <repositories>
                <repository>
                    <type>com.greenpepper.runner.repository.AtlassianRepository</type>
                    <root><![CDATA[http://localhost:19005/rpc/xmlrpc?handler=greenpepper1#SPACE KEY;login;password;true]]></root>
                    <name>atlrepo</name>
                    <projectName>PROJECT</projectName>
                    <systemUnderTest>SUTNAME</systemUnderTest>
                </repository>
            </repositories>
        <configuration>
    <plugin>
</plugins>
```


### If the repository doesn't require an authentication

```xml
<plugins>
    <plugin>
        <groupId>com.github.strator-dev.greenpepper</groupId>
        <artifactId>greenpepper-maven-plugin</artifactId>
        <version>${project.version}</version>
        <configuration>
            <repositories>
                <repository>
                    <type>com.greenpepper.runner.repository.AtlassianRepository</type>
                    <root><![CDATA[http://localhost:19005/rpc/xmlrpc?handler=greenpepper1#SPACE KEY;;;true]]></root>
                    <name>atlrepo</name>
                    <projectName>PROJECT</projectName>
                    <systemUnderTest>SUTNAME</systemUnderTest>
                </repository>
            </repositories>
        <configuration>
    <plugin>
</plugins>
```

