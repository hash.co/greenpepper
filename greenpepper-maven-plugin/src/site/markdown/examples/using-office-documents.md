# Using OpenOffice ODT, MS word .doc or .docx 

It's possible to write the specification in a OpenDocument file (MS word, ODT). 
For running the specification, GreenPepper will need a class that implements the `SpecificationDialect` 
interface. That class should be able to translate from the documents to HTML. 

GreenPepper extensions provides an implementation for the 3 syntaxes: 
  * .doc: `com.greenpepper.runner.dialect.DocDialect`
  * .docx: `com.greenpepper.runner.dialect.DocxDialect`
  * .odt: `com.greenpepper.runner.dialect.OdtDialect`

Here is the configuration needed for supporting these formats.

```xml
<plugin>
    <groupId>com.github.strator-dev.greenpepper</groupId>
    <artifactId>greenpepper-maven-plugin</artifactId>
    <version>${project.version}</version>
    <configuration>
        ...
        <repositories>
            <repository>
                <name>MS DOCX specs</name>
                <root>src/test/specs;docx</root>
                <dialect>com.greenpepper.runner.dialect.DocxDialect</dialect>
            </repository>
            <repository>
                <name>MS DOC specs</name>
                <root>src/test/specs;doc</root>
                <dialect>com.greenpepper.runner.dialect.DocDialect</dialect>
            </repository>
            <repository>
                <name>OpenOffice ODT specs</name>
                <root>src/test/specs;odt</root>
                <dialect>com.greenpepper.runner.dialect.OdtDialect</dialect>
            </repository>
        </repositories>
    </configuration>
    ....
</plugin>
```

There are 2 points to notice in the above configuration: 

### The dialect definition in the repositories

```xml
    <dialect>com.greenpepper.runner.dialect.DocxDialect</dialect>
```

Every file found in the corresponding repository will be processed by the dialect.

### The file extension filtering for FileSystemRepository

> **Note:** the default type for a repository is the `FileSystemRepository` hence our ommitting of that setting.

```xml
<repository>
    <root>src/test/specs;docx</root>
</repository>
```

The default behavior of the **FileSystemRepository** is to look for `.html` files. 
But you will usually write your documents with `.doc`, `.docx`, `.odt` files. So we need to tell the 
repository to filter on `.doc` by setting a second arguments in the constructor (found in the `<root>` tag).

## FAQ

### Illegal zip format when using DOCX specification

The **Docx** format is in fact a zip of XML files. So it's normal to ear about zip file 
when working with `.docx`. 

However, it's important to avoid the Maven filtering on those files. So if you have this error
check for the filtering setting on your maven project. When Maven filters the `.docx` files, it
corrupts them.

Here is an example configuration that will avoid the corruption:
```xml
<testResources>
    <testResource>
        <directory>src/test/resources</directory>
        <filtering>true</filtering>
        <excludes>
            <exclude>**/*.docx</exclude>
            <exclude>**/*.doc</exclude>
            <exclude>**/*.odt</exclude>
        </excludes>
    </testResource>
    <testResource>
        <directory>src/test/resources</directory>
        <filtering>false</filtering>
        <includes>
            <include>**/*.docx</include>
            <include>**/*.doc</include>
            <include>**/*.odt</include>
        </includes>
    </testResource>
</testResources>
```