# Using Markdown 

It's possible to write the specification in wiki syntax. For running the specification, GreenPepper will need a class that
implements the `SpecificationDialect` interface. That class should be able to translate from the wiki syntax to HTML. 

GreenPepper extensions provides an implementation for the Markdown syntax: `com.greenpepper.runner.dialect.MarkdownDialect`

Here is the configuration needed for the Markdown support.

```xml
<plugin>
    <groupId>com.github.strator-dev.greenpepper</groupId>
    <artifactId>greenpepper-maven-plugin</artifactId>
    <version>${project.version}</version>
    <configuration>
        ...
        <repositories>
            <repository>
                <type>com.greenpepper.repository.FileSystemRepository</type>
                <root>${basedir}/src/specs/greenpepper;md</root>
                <name>GreenPepper-specifications</name>
                <dialect>com.greenpepper.runner.dialect.MarkdownDialect</dialect>
            </repository>
        </repositories>
    </configuration>
    ....
</plugin>
```

There are 2 points to notice in the above configuration: 

## The dialect definition in the repository

```xml
    <dialect>com.greenpepper.runner.dialect.MarkdownDialect</dialect>
```

Every file found in the repository will be processed by the dialect.

## The file extension filtering for FileSystemRepository

```xml
<repository>
    <type>com.greenpepper.repository.FileSystemRepository</type>
    <root>${basedir}/src/specs/greenpepper;md</root>
</repository>
```

The default behavior of the **FileSystemRepository** is to look for `.html` files. 
But you will usually write your Markdown specifications in `.md` files. So we need to tell the 
repository to filter on `.md` by setting a second arguments in the constructor (found in the `<root>` tag).
