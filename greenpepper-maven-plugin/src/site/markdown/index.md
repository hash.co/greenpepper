{toc}

# Using the releases version

> **tip:**
> 
> There is no need for any Maven setup to use the releases of the GreenPepper Maven plugin. The binaries are already uploaded to Central 

# Use the development version of the GreenPepper Maven plugin

You will need to set your Maven up to access OSS repository in for **settings.xml** .

```xml
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
    <profiles>
        <profile>
            <id>ossrh</id>
            <repositories>
                <repository>
                    <id>ossrh</id>
                    <name>Sonatype Public Repository</name>
                    <url>https://oss.sonatype.org/content/groups/public/</url>
                </repository>
            </repositories>
            <pluginRepositories>
                <pluginRepository>
                    <id>ossrh</id>
                    <name>Sonatype Public Repository</name>
                    <url>https://oss.sonatype.org/content/groups/public/</url>
                </pluginRepository>
            </pluginRepositories>
        </profile>
    </profiles>
    <activeProfiles>
        <activeProfile>ossrh</activeProfile>
    </activeProfiles>
</settings>
```


# Usage

The GreenPepper Plugin is used during the `integration-test` phase of the build lifecycle to execute the integration 
tests of an application. It generates reports  in 2 different file formats:

* HTML files (*.html)
* XML files (*.xml)

By default, these files are generated at `${basedir}/target/greenpepper-reports`.

General instructions on how to use the GreenPepper Plugin can be found on the [usage page]. Some more specific use cases are described in the examples given below.

----

| Support | Issues | Contributions |
|:---------:|:--------:|:--------------:|
| ![support.png] | ![questions.png] | ![contribute.png] |
| In case you still have questions regarding the plugin's usage, please have a look at the [FAQ] and feel free to contact the [user chat room] on ![Gitter]. | If you feel like the plugin is missing a feature or has a defect, you can fill a feature request or bug report in our [issue tracker]. Questions can also be asked there.When creating a new issue, please provide a comprehensive description of your concern. Especially for fixing bugs it is crucial that the developers can reproduce your problem. For this reason, entire debug logs, POMs or most preferably little demo projects attached to the issue are very much appreciated. | Of course, patches are welcome, too. Contributors can clone the project from our [source repository] and will find supplementary information in the [guide to Developing GreenPepper]. |


----

${plugin.goals}

----


# Examples

The following examples show how to use the Surefire Plugin in more advanced use-cases:

{children:sort=title|page=GreenPepper Maven Plugin - Examples}


[usage page]: Usage
[Gitter]: https://img.shields.io/gitter/room/strator-dev/greenpepper.svg?maxAge=2000
[user chat room]: https://gitter.im/strator-dev/greenpepper
[issue tracker]: https://github.com/strator-dev/greenpepper/issues
[source repository]: https://github.com/strator-dev/greenpepper
[guide to Developing GreenPepper]: https://greenpepper.atlassian.net/wiki/display/DOC/Contribute+to+Code
