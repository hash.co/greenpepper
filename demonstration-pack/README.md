# GreenPepper 

## The Demonstration pack

This pack contains :
  
  * **greenpepper-client-@@project.version@@.jar** : The command line client for greenpepper. This client differs from the `greenpepper-core.jar`
    in the fact that it includes the `greenpepper-extensions-java.jar`.
  * **greenpepper-samples-application-@@project.version@@.jar** : the demonstration application classes.
  * **greenpepper-samples-application-@@project.version@@-sources.jar** : the demonstration application classes sources.
  * **project/src/fixture/specs** : the demo HTML specification.
  * **project/src/fixture/mdSpecs** : the demo Markdown specification.

## Command line runner

```
java -cp \
greenpepper-client-*.jar:greenpepper-samples-application-*.jar \
com.greenpepper.runner.Main --help
```

### Launch a Test

#### HTML Specifications

To run the HTML specification, run for instance 

```sh
./run.sh project/src/fixture/specs/Demo/Bank.html 
```

#### Markdown Specification

The Markdown Specification needs a dialect to be able to understand Markdown.

```
./run.sh -d com.greenpepper.runner.dialect.MarkdownDialect project/src/fixture/mdSpecs/Calculator.md 
```

> The result will be in the current directory.
>
> The `run.sh` is just a shortcut for the 'java' command with the class path.

## Maven Plugin

Go into the `project` folder and launch `mvn integration-test`

```
cd project
mvn integration-test
```

